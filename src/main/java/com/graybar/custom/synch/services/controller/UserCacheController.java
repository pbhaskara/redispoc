package com.graybar.custom.synch.services.controller;

import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graybar.custom.synch.services.entity.User;
import com.graybar.custom.synch.services.service.UserCacheService;

@RestController
@RequestMapping("/users")
public class UserCacheController {

	@Autowired
	UserCacheService userCacheService;

	@GetMapping("/")
	public ResponseEntity<Map<Long, User>> getAllUsers() {
		return new ResponseEntity<>(userCacheService.findAll(), HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<Set<User>> saveUser(@RequestBody User user) {
		userCacheService.save(user);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@GetMapping("/{id}")
	public ResponseEntity<User> getUser(@PathVariable Long id) {
		User user = userCacheService.find(id);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		User user = userCacheService.find(id);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		userCacheService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/")
	public ResponseEntity<Void> deleteAll() {
		userCacheService.deleteAll();
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
