package com.graybar.custom.synch.services.service;

import java.util.Map;

import com.graybar.custom.synch.services.entity.User;

public interface UserCacheService {

	Map<Long, User> findAll();

	void save(User user);

	User find(Long id);

	void delete(Long id);

	void deleteAll();
}
