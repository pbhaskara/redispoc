package com.graybar.custom.synch.services.repository;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.stereotype.Repository;

import com.graybar.custom.synch.services.entity.User;

@Repository
public class UserCacheRepositoryImpl implements UserCacheRepository {

	private static final String KEY = "user";

	@Resource(name = "userRedisTemplate")
	private HashOperations<String, String, String> hashOperations;

	public void save(User user, String userJson) {
		hashOperations.put(KEY, String.valueOf(user.getId()), userJson);
	}

	public String find(Long id) {
		return hashOperations.get(KEY, String.valueOf(id));
	}

	public Map<String, String> findAll() {
		return hashOperations.entries(KEY);
	}

	public void deleteAll() {
		hashOperations.delete(KEY, hashOperations.keys(KEY).toArray());
	}

	public void delete(Long id) {
		hashOperations.delete(KEY, String.valueOf(id));
	}
}
