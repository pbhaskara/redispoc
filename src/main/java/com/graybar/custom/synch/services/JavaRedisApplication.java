package com.graybar.custom.synch.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.graybar.custom.synch.services" })
@EnableAutoConfiguration
public class JavaRedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaRedisApplication.class, args);
	}
}
