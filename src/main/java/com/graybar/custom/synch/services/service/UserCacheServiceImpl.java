package com.graybar.custom.synch.services.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.graybar.custom.synch.services.entity.User;
import com.graybar.custom.synch.services.repository.UserCacheRepository;

@Service
public class UserCacheServiceImpl implements UserCacheService {

	@Autowired
	private UserCacheRepository userCacheRepository;
	
	@Autowired
	private Gson gson;

	@Override
	public Map<Long, User> findAll() {
		Map<String, String> entries = userCacheRepository.findAll();
		Map<Long, User> mapCopy = new HashMap<>(entries.size());
		for (String key : entries.keySet()) {
			mapCopy.put(Long.valueOf(key), gson.fromJson(entries.get(key), User.class));
		}
		return mapCopy;
	}

	@Override
	public void save(User user) {
		userCacheRepository.save(user, gson.toJson(user));
	}

	@Override
	public User find(Long id) {
		return gson.fromJson(userCacheRepository.find(id), User.class);
	}

	@Override
	public void delete(Long id) {
		userCacheRepository.delete(id);
	}

	@Override
	public void deleteAll() {
		userCacheRepository.deleteAll();
	}

}
