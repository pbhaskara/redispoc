package com.graybar.custom.synch.services.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@Configuration
@EnableRedisRepositories
public class RedisConfig {

	private @Value("${redis.host}") String redisHost;
	
	private @Value("${redis.port}") int redisPort;
	
	private @Value("${redis.password}") String redisPwd;
	
	private @Value("${redis.db1}") int redisDbInstance;

	@Bean
	public LettuceConnectionFactory redisConnectionFactory() {
		RedisStandaloneConfiguration redisStandalone = new RedisStandaloneConfiguration(redisHost, redisPort);
		redisStandalone.setDatabase(redisDbInstance);
		redisStandalone.setPassword(RedisPassword.of(redisPwd));
		return new LettuceConnectionFactory(redisStandalone);
	}
	
	@Bean
	public StringRedisTemplate userRedisTemplate() {
		StringRedisTemplate redisTemplate = new StringRedisTemplate();
		redisTemplate.setConnectionFactory(redisConnectionFactory());
		return redisTemplate;
	}

}
