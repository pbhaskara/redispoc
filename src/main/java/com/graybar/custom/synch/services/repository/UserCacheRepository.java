package com.graybar.custom.synch.services.repository;

import java.util.Map;

import com.graybar.custom.synch.services.entity.User;

public interface UserCacheRepository {

	Map<String, String> findAll();

	void save(User user, String userString);

	String find(Long id);

	void delete(Long id);

	void deleteAll();

}
